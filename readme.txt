Rationale:
- Initially I was going to input the data into the program via direct typing into the console, however, afterwards I decided to implement the direct file input method due to how it makes inserting the variables into the program easier and quicker.
- Giving the end program as an .jar file is designed for a potential situation where the tester does not have Eclipse Java Photon on their device, because Java was not one of the programming languages listed on the job specifications and Java is the language I am most experienced with.
- In terms of testing I used JUnit as it is Eclipse's inbuilt automatic testing framework. With it I made 7 cases designed to test if the application worked and gave the correct values. (Which were found independently using the "Simple tax calculator" below)
	- I also made another 5 bad test cases designed to make sure that the failure conditions failed. Unfortunately, with the way I designed things in the program meant that only 1 of these could be tested at a time.
- In terms of these failure conditions, it was designed so that if there was something wrong with the input data (Negative numbers, Numbers instead of letters) the application would quit immediately as there would not be any use in continuing without the correct data.

Assumptions:
- Any programming language was available to use.
- That payment dates were 1 month only
- The payment dates were within the same year. Furthermore:
	- The payment dates are all correct. (e.g. No 00 April - 31 April)
	- The payment dates were using the entire month (1st March - 31st March, not 15th March - 20th March)
	- The input syntax was the same as the example solution (FName, LName, YearSalary, Super, Payment Dates)
- Only final output required rounding to the nearest dollar
- The 2nd Example solution was incorrect with the amount of income-tax. 
	- I tested the actual amount using 'https://www.ato.gov.au/Calculators-and-tools/Simple-tax-calculator/' and got 32,032.00 where 32,032.00/12 = 2669


How to use:
- To use the TechTest.jar file, Powershell is used by opening it where the TechTest.jar file is.
	- Then by using the command 'java -jar TechTest.jar'.
	- The output values of the program is seen within the output.txt file and the input variables can be changed in the input.txt file.

- To use the source code, Eclipse Java Photon is required.
	- This requires the archive file "IPG Technical Test.zip" to be imported into Eclipse.
	- The source code also comes with the JUnit tests, which is the only purpose of the source code version (to run the JUnit tests).


Bitbucket Repository Link:
https://bitbucket.org/chrisone4/ipg-technical-test/src