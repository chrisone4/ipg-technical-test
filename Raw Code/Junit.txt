import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;


class Junit {

	@Test
	void testNormalCases() throws UnsupportedEncodingException {
		
		//Example 1 Test Case
	    List<String> input1 = Arrays.asList("Andrew", "Smith", "60050", "9%", "01 March – 31 March");
	    String expected1 = "Andrew Smith, 01 March – 31 March, 5004, 922, 4082, 450";
	    
	    assertThat(Main.testMain(input1), containsString(expected1));
	    
	    //Example 2 Test Case
	    List<String> input2 = Arrays.asList("Claire", "Wong", "120000", "10%", "01 March – 31 March");
	    String expected2 = "Claire Wong, 01 March – 31 March, 10000, 2669, 7331, 1000";
	    
	    assertThat(Main.testMain(input2), containsString(expected2));
	    
	    //If annual salary is below 18,000
	    List<String> input3 = Arrays.asList("Jonny", "Decker", "9000", "11%", "01 April – 30 April");
	    String expected3 = "Jonny Decker, 01 April – 30 April, 750, 0, 750, 83";
	    
	    assertThat(Main.testMain(input3), containsString(expected3));
	    
	    //If annual salary is exactly 18200
	    List<String> input4 = Arrays.asList("Sian", "Dixon", "18200", "9%", "01 April – 30 April");
	    String expected4 = "Sian Dixon, 01 April – 30 April, 1517, 0, 1517, 137";
	    
	    assertThat(Main.testMain(input4), containsString(expected4));
	    
	    //If annual salary is exactly 23000
	    List<String> input5 = Arrays.asList("Olivier", "Evans", "23000", "8%", "01 April – 30 April");
	    String expected5 = "Olivier Evans, 01 April – 30 April, 1917, 76, 1841, 153";
	    
	    assertThat(Main.testMain(input5), containsString(expected5));
	    
	    //If annual salary is exactly 18205
	    List<String> input6 = Arrays.asList("Lee", "Jennings", "18205", "8%", "01 April – 30 April");
	    String expected6 = "Lee Jennings, 01 April – 30 April, 1517, 0, 1517, 121";
	    
	    assertThat(Main.testMain(input6), containsString(expected6));
	    
	    //If annual salary is above 180,001
	    List<String> input7 = Arrays.asList("Charles", "Pearson", "190000", "12%", "01 April – 30 April");
	    String expected7 = "Charles Pearson, 01 April – 30 April, 15833, 4894, 10939, 1900";
	    
	    assertThat(Main.testMain(input7), containsString(expected7));	 
	}
	
	@Test
	void testBadCases() throws UnsupportedEncodingException {
		//One at a time
		/*
		//If annual salary contains letters
	    List<String> input1 = Arrays.asList("Andrew", "Smith", "abcd", "8%", "01 March – 31 March");
	    String expected1 = "Invalid Input for Annual Salary ('0-9' allowed). Please try again";
	    
	    assertThat(Main.testMain(input1), containsString(expected1));
	    
		//If annual salary is below 0
	    List<String> input2 = Arrays.asList("Andrew", "Smith", "-1", "8%", "01 March – 31 March");
	    String expected2 = "Annual Salary out of bounds (> 0 allowed). Please try again";
	    
	    assertThat(Main.testMain(input2), containsString(expected2));
		
		//If Super rate is above 12
	    List<String> input3 = Arrays.asList("Andrew", "Smith", "60050", "13%", "01 March – 31 March");
	    String expected3 = "Super rate out of bounds (0% - 12%). Please try again";
	    
	    assertThat(Main.testMain(input3), containsString(expected3));
	    
		
		//If Super rate is below 0
	    List<String> input4 = Arrays.asList("Andrew", "Smith", "60050", "-1%", "01 March – 31 March");
	    String expected4 = "Super rate out of bounds (0% - 12%). Please try again";
	    
	    assertThat(Main.testMain(input4), containsString(expected4));
	    
		
	    List<String> input5 = Arrays.asList("Andrew", "Smith", "60050", "-abcd%", "01 March – 31 March");
	    String expected5 = "Invalid Input for Super Rate ('0-9' and '%' allowed). Please try again";
	    
	    assertThat(Main.testMain(input5), containsString(expected5));
	    */
	}
}
